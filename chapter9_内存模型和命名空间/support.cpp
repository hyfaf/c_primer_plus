// support.cpp引用了外部变量，同时定义了update()、local()
#include<iostream>
extern double warming;

void update(double dt);
void local();

using std::cout;
void update(double dt)
{
    extern double warming; 
    warming += dt;
    cout << "Updateing global warming to " << warming;
    cout << " degrees.\n";
}

void local()
{
    double warming = 0.8;       // 临时变量覆盖了全局变量

    cout << "Local warming = " << warming << " degrees.\n";
    cout << "But global warming = " << ::warming;   // C++中的临时变量表示法
    cout << " degrees.\n";
}
