// 对coordin.h头文件中函数的具体实现
#include<iostream>
#include<cmath>
#include"coordin.h"

// 直角坐标系转极坐标系
polar rect_to_polar(rect xypos)
{
    using namespace std;
    polar answer;

    answer.distance = 
        sqrt( xypos.x * xypos.x + xypos.y * xypos.y);
    answer.angle = atan2(xypos.y, xypos.x);

    return answer;
}

// 显示转换后的极坐标
void show_polar(polar dapos)
{
    using namespace std;
    const double Rad_to_deg = 57.29577951;

    cout << "distance = " << dapos.distance;
    cout << ", angle = " << dapos.angle * Rad_to_deg;
    cout << " degrees\n";
}