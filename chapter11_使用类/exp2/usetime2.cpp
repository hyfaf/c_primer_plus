// 使用运算符重载
#include<iostream>
#include"mytime2.h"

int main()
{
    using std::cout;
    using std::endl;
    Time weeding(4, 35);
    Time waxing(2, 47);
    Time total;
    Time diff;
    Time adjusted;

    cout << "weeding time = ";
    weeding.Show();
    cout << endl;

    cout << "waxing time = ";
    waxing.Show();
    cout << endl;

    cout << "total work time = ";
    total = weeding + waxing;   // 使用了+运算符重载
    total.Show();
    cout << endl;

    diff = weeding - waxing;    // 使用了-运算符重载
    cout << "weeding time - waxing time = ";
    diff.Show();
    cout << endl;

    adjusted = total * 1.5;     // 使用了*运算符重载
    cout << "adjusted work time = ";
    adjusted.Show();
    cout << endl;
    return 0;

    return 0;
}