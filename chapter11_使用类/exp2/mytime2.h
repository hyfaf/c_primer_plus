// 运算符重载示例，类声明中有一个运算符重载函数
#ifndef MYTIME2_H_
#define MYTIME2_H_

class Time
{
private:
    int hours;
    int minutes;
public:
    Time();
    Time(int h, int m = 0);
    void AddMin(int m);
    void AddHr(int h);
    void Reset(int h = 0, int m = 0);
    Time operator+(const Time & t) const;   // 运算符+号重载
    Time operator-(const Time & t) const;   // 运算符-号重载
    Time operator*(double n) const;   // 运算符*号重载
    void Show() const;
};

#endif