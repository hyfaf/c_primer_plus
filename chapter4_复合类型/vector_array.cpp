#include<iostream>
#include<vector>
#include<array>
using namespace std;
int main()
{
    int a[] = {2,3,4,5,6};
    vector<int> vec = {1, 2, 3, 4, 5};
    array<int,5> arr = {2,5,8,9};

    // a[-2]被转换成了*(a-2),这将导致数组越界赋值，很危险，不要这样做。（虽然这样做很像Python中的用法）
    // 此时编译器也不会报错，要注意！
    cout << "one way:" << a[2] << " another way: " << a[-2] << endl;

    // 对其他容器也不要这样，这只是演示
    cout << "one way:" << vec[2] << " another way: " << vec[-3] << endl;
    cout << "one way:" << arr[2] << " another way: " << arr[-2] << endl;

    // 若需要编译器判断是否安全，可以使用成员函数at()，会终止报错。但只是用于容器类
    cout << vec.at(-3) << endl;
    cout << arr.at(-2) << endl;

    return 0;
}