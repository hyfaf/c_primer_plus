#include<iostream>
#include<string>

int main()
{
    using namespace std;

    int* num = new int [10];
    cout << num << " " <<  &num << endl;
    cout << ++num << " " <<  &num << endl;
    cout << ++num << " " <<  &num << endl;
    cout << ++num << " " <<  &num << endl;

    delete [] num;  // 指针指向数组时，需要这样释放
    return 0;
}