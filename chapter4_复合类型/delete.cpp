#include<iostream>
#include<cstring>
using namespace std;
char * getname(void);
int main()
{
    char * name;

    name = getname();
    cout << name << " at " << (int *)name << "\n";
    delete [] name;

    // 重复使用char *
    name = getname();
    cout << name << " at " << (int *)name << "\n";
    delete [] name;
    return 0;
}

char * getname()
{
    char temp[80];
    cout << "Enter last name: ";
    cin >> temp;
    char * pn = new char[strlen(temp) + 1];     // 这里有个疑问，二次new申请内存时分配的是同一块内存地址
    strcpy(pn, temp);

    // temp数组只是临时储存，函数返回pn时会释放内存
    return pn;
}