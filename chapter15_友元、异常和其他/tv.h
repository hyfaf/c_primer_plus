#ifndef TV_H_
#define TV_H_

class Tv
{
public:
    friend class Remote;    // 友元类Remote,可以访问Tv类的私有部分
    enum {Off, On};
    enum {MinVal,MaxVal = 20};
    enum {Antenan, Cable};
    enum {TV, DVD};

    Tv(int s = Off, int mc = 125): state(s), volume(5),
        maxchannel(mc), channel(2), mode(Cable), input(TV) {}
    void onoff() {state = (state == On)? Off : On;}
    bool ison() const {return state == On;}
    bool volup();
    bool voldown();
    void chanup();
    void chandown();
    void set_mode() {mode = (mode == Antenan)? Cable : Antenan;}
    void set_input() {input = (input == TV)? DVD : TV;}
    void settings() const;   // 现实所有的设置
private:
    int state;      // 电视类的状态{Off, On}
    int volume;     // 电视类的音量
    int maxchannel;     // 电视类的频道数
    int channel;        // 最近的频道设置
    int mode;       // 电视类的模式{broadcast or cable}
    int input;      // 电视类的输入{TV or DVD}
};

// 友元类Remote声明,相当一个遥控器类控制tv
class Remote    
{
private:
    int mode;   // 控制输入模式是TV或是DVD
public:
    Remote(int m = Tv::TV) : mode(m) {}
    bool volup(Tv & t) { return t.volup(); }
    bool voldown(Tv & t) { return t.voldown(); }
    void onoff(Tv & t) { t.onoff(); }
    void chanup(Tv & t) { t.chanup(); }
    void chandown(Tv & t) { t.chandown(); }
    void set_chan(Tv & t, int c) { t.channel = c; }
    void set_mode(Tv & t) { t.set_mode(); }
    void set_input(Tv & t) { t.set_input(); }
};



#endif