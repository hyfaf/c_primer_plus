// 第12章一个修复过后的自定义string类
#ifndef STRING1_H_
#define STRING1_H_
#include<iostream>
using std::ostream;
using std::istream;

class String
{
private:
    char * str;                     // 记录字符串的内容
    int len;                        // 记录字符串的长度
    static int num_string;          // 类的静态变量，记录有多少个字符串
    static const int CINLIM = 80;   // 类的静态常量

public:
    String(const char * s);     // 构造函数
    String();                   // 默认构造函数
    String(const String &);     // 复制构造函数
    ~String();                  // 析构函数
    int length() const { return len; };     // 计算字符串长度的方法

    // 运算符重载
    String & operator=(const String &);     // 赋值运算符重载
    String & operator=(const char *);
    char & operator[](int i);
    const char & operator[](int i) const;

    // 重载友元运算符
    friend bool operator<(const String &st, const String &st2);
    friend bool operator>();


    // 类的静态函数
    static int HowMany();
};
#endif