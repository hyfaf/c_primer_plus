#include<iostream>
#include<ctime>
double square(double x){ return x * x; }

int main()
{
    clock_t start_t, end_t;
    start_t = clock();
    using namespace std;
    double a, b;
    double c = 13.0;

    a = square(5.0);
    b = square(4.5 + 7.5);
    cout << "a = " << a << ", b = " << b << "\n";
    cout << "c = " << c;
    cout << ", c square = " << square(c++) << "\n";
    cout << "Now c = " << c << "\n";
    end_t = clock();
    cout << "Time Spend:" << end_t - start_t << "ms"<< endl;
    return 0;
}