// 随便写一个函数重载的例子, 顺便练习一下引用的使用
#include<iostream>

struct vector
{
    double x;
    double y;
};

int add(int a, int b);
void add(const vector & vec1, const vector & vce2, vector & res);

int main()
{
    using namespace std;

    int a = 3, b = 4;
    int result;
    vector vec1 = { 5.0, 6.0 };
    vector vec2 = { 7.0, 8.0 };
    vector res;

    result = add(a, b);
    cout << "a + b = " << result << endl;

    add(vec1, vec2, res);
    cout << "res.x = " << res.x << endl;
    cout << "res.y = " << res.y << endl;

    cout << "vec1.x = " << vec1.x << endl;
    cout << "vec1.y = " << vec1.y << endl;

    return 0;
    
}

int add(int a, int b)
{
    return a + b;
}

// 涉及复杂类型修改的最好用引用传入，否则函数内部不好处理
void add(const vector & vec1, const vector & vec2, vector & res)
{
    res.x = vec1.x + vec2.x;
    res.y = vec1.y + vec2.y;
}