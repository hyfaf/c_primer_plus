#include<iostream>
int main()
{
    using namespace std;
    int rats = 101;
    int * pt = &rats;
    int & rodents = *pt;
    cout << "rats = " << rats << "      rats address = " << &rats << endl;
    cout << "pt_value = " << *pt << "       pt address = " << pt << endl;
    cout << "rodents = " << rodents << "        rodents address = " << &rodents << endl;

    cout << endl;

    int bunnies = 50;
    pt = &bunnies;
    cout << "rats = " << rats << "      rats address = " << &rats << endl;
    cout << "pt_value = " << *pt << "       pt address = " << pt << endl;
    cout << "rodents = " << rodents << "        rodents address = " << &rodents << endl;    // 通过指针也不能更改引用
    cout << "bunnies = " << bunnies << "        bunnies address = " << &bunnies << endl;

    return 0;
}