#include<iostream>
using namespace std;

void song(const char* name = "I Love you", int times = 5)
{
    for (int i = times; i > 0; i--)
        cout << name << endl;
}

int main()
{
    song();
    song("Hello World!", 3);

    return 0;
}
