#include"tabtenn0.h"
#include<iostream>

// 列表初始化语法
TableTennisPlayer::TableTennisPlayer(const string & fn,
    const string & ln, bool ht) : firstname(fn), lastname(ln), hasTable(ht) {}

/* 
或者，你也可以按构造函数的方式初始化变量
TableTennisPlayer::TableTennisPlayer(const string & fn,
    const string & ln, bool ht)
{
    firstname = fn;
    lastname = ln;
    hasTable = ht;
}
*/

void TableTennisPlayer::Name() const
{
    std::cout << lastname << ", " << firstname;
}