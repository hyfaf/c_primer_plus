#include"tabtenn1.h"
#include<iostream>

// 列表初始化语法
TableTennisPlayer::TableTennisPlayer(const string & fn,
    const string & ln, bool ht) : firstname(fn), lastname(ln), hasTable(ht) {}

/* 
或者，你也可以按构造函数的方式初始化变量
TableTennisPlayer::TableTennisPlayer(const string & fn,
    const string & ln, bool ht)
{
    firstname = fn;
    lastname = ln;
    hasTable = ht;
}
*/

void TableTennisPlayer::Name() const
{
    std::cout << lastname << ", " << firstname;
}

// RatedPlayer 派生类方法定义
// RatedPlayer 派生类构造函数
RatedPlayer::RatedPlayer(unsigned int r, const string & fn,
    const string & ln, bool ht) : TableTennisPlayer(fn, ln, ht)
{
    rating = r;
}

// RatedPlayer 派生类复制构造函数，从基类中复制信息
RatedPlayer::RatedPlayer(unsigned int r, const TableTennisPlayer & tp)
    : TableTennisPlayer(tp), rating(r)
{
}