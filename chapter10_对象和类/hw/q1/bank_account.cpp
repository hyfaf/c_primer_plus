#include<iostream>
#include"bank_account.h"

BankAccount::BankAccount(string name, string account, unsigned long deposit)
{
    this->name = name;
    this->account = account;
    this->deposit = deposit;
}

void BankAccount::show_account() const
{
    cout << "Accout name: " << this->name << endl;
    cout << "Account: " << this->account << endl;
    cout << "Account deposit: " << this->deposit << endl;
}

bool BankAccount::save_money(unsigned long value)
{
    this->deposit += value;
    return true;
}

bool BankAccount::draw_money(unsigned long value)
{
    if(deposit - value < 0)
        return false;
    else
    {
        deposit -= value;
        return true;
    }
}