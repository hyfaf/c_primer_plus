#ifndef BANK_ACCOUNT_H
#define BANK_ACCOUNT_H

#include<string>
using namespace std;        // 使用标准string，必须添加

class BankAccount
{
private:
    string name;
    string account;
    unsigned long deposit;
public:
    BankAccount(string name, string account, unsigned long deposit = 0);                  // 初始化银行账户
    void show_account() const;            // 显示账户信息
    bool save_money(unsigned long value);  // 将指定数目存入账户
    bool draw_money(unsigned long value);  // 将指定数目取出账户
};

#endif