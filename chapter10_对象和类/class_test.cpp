#include<iostream>
using namespace std;

class Human
{
private:
    const int a = 10;
public:    // 构造函数和虚构函数需要设为公共类型，否则无法从外部进行对象的初始化
    Human()
    {
        cout << "Create a Human object!" << endl;
    }

    ~Human()
    {
        cout << "Delete a Human object!" << endl;
    }
};

int main()
{
    Human jason;

    return 0;
}