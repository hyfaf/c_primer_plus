// 调用栈的主函数
// 模拟一个栈存储数字，键入a(A)输入一个数字压入栈，键入p(P)从栈中弹出一个数字， 键入q(Q)退出程序
#include<iostream>
#include<cctype>
#include"stack.h"

int main()
{
    using namespace std;
    Stack st;
    char ch;
    unsigned long po;
    cout << "Please enter A to add a purchase order,\n"
        << "P to process a PO, or Q to quit.\n";
    while(cin >> ch && toupper(ch) != 'Q')
    {
        while(cin.get() != '\n')    // 接收第一次输入时存入缓冲区的换行符，否则后续无法正确输入
            continue;
        if (!isalpha(ch))
        {
            cout << '\a';
            continue;
        }
        switch(ch)
        {
            case 'A':
            case 'a': cout << "Enter a PO number to add: ";
                      cin >> po;
                      if(st.isfull())
                        cout <<"stack already full\n";
                      else
                        st.push(po);
                      break;
            case 'P':
            case 'p': if (st.isempty())
                        cout << "stack already empty\n";
                        else {
                            st.pop(po);
                            cout << "PO #" << po << " popped\n";
                        }
                        break;
        }
        cout << "Please enter A to add a purchase order,\n"
             << "p to process a PO, or Q to quit.\n";

    }
    cout << "Bye\n";
    return 0;

}