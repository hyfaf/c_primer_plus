// 对stack.h中栈的成员函数具体描述
#include<iostream>
#include "stack.h"

Stack::Stack()
{
    top = 0;    // 初始化栈，一开始为空栈
}

bool Stack::isempty() const
{
    return top == 0;    // 这样写更简洁，避免使用if
}

bool Stack::isfull() const
{
    return top == MAX;
}

bool Stack::push(const Item & item)
{
    if(isfull())    //先判断栈是否已满
        return false;
    else
    {
        items[top++] = item;
        return true;
    }

}

bool Stack::pop(Item & item)
{
    if(isempty())   // 先判断栈是否已空
        return false;
    else
    {
        item = items[--top];    // 使用数组表示栈时，只需要关注栈此时的有效栈层
        return true;            // item此时为栈最顶层的数
    }

}