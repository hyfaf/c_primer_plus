/* 一个函数指针的数组例子,分别展示了C++11下方便的做法，以及C++98下的老式做法 */
#include<iostream>

// 三种不同的声明，一样的效果
const double * f1(const double ar[], int n);
const double * f2(const double [], int);
const double * f3(const double *, int);


int main()
{
    using namespace std;
    double av[3] = {1112.3, 1542.6, 2227.9};

    // pointer to a function
    const double *(*p1)(const double *, int) = f1;  // C++98的做法
    auto p2 = f2;   // C++11的自动推导类型做法

    cout << "Using pointers to function:\n";
    cout << "Address Value\n";
    cout << (*p1)(av,3) << ": " << *(*p1)(av,3) << endl;
    cout << p2(av, 3) << ": " << *p2(av, 3) << endl;

    // C++11自动推导类型不可用于列表初始化
    const double *(*pa[3])(const double *, int) = {f1,f2,f3};
    // 但初始化了一次后则可以在别的情况下使用
    auto pb = pa;
    cout << "\nUsing an array of pointers to functions:\n";
    cout << " Address Value\n";
    for (int i = 0; i < 3; i++)
        cout << pa[i](av,3) << ": " << *pa[i](av,3) << endl;
    cout << "\nUsing a pointer to a pointer to a function:\n";
    cout << " Address Value\n";
    for (int i = 0; i < 3; i++)
        cout << pb[i](av,3) << ": " << *pb[i](av,3) << endl;

    cout << "\nUsing pointers to an array of pointers:\n";
    cout << " Adress Value\n";

    auto pc = &pa;
    cout << (*pc)[0](av,3) << ": " << *(*pc)[0](av,3) << endl;

    const double *(*(*pd)[3])(const double *, int) = &pa;

    const double *pdb = (*pd)[1](av,3);
    cout << pdb << ": " << *pdb << endl;

    cout << (*(*pd)[2])(av,3) << ": " << *(*(*pd)[2])(av,3) << endl;
    return 0;
}


const double * f1(const double * ar, int n)
{
    return ar;
}

const double * f2(const double ar[], int n)
{
    return ar+1;
}
const double * f3(const double ar[], int n)
{
    return ar+2;
}