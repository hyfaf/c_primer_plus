// 函数指针的使用示例
// 该程序描述：你给出一个code行的代码，betsy和pam分别需要花费多久的开发时间
#include<iostream>
double betsy(int);
double pam(int);

void estimate(int lines, double (*pf)(int));

int main()
{
    using namespace std;
    int code;
    cout << "How many lines of code do you need? ";
    cin >> code;
    cout << "Here's Besty's estimate:\n";
    estimate(code, betsy);
    cout << "Here's Pam's estimate:\n";
    estimate(code, pam);
    return 0;
}

double betsy(int lns)
{
    return 0.05 * lns;
}

double pam(int lns)
{
    return 0.03 * lns + 0.0004 * lns * lns;
}

/*
    estimate函数的第二个参数为一个double型的函数指针
        函数的类型  函数内参数的类型
        |          |
       ∨          ∨
    double (*pf)(int))
*/
void estimate(int lines, double (*pf)(int))     
{
    using namespace std;
    cout << lines << " lines will take ";
    cout << (*pf)(lines) << " hour(s)\n";
}