#include<iostream>
using namespace std;

int main()
{
    const int a = 80;
    int b = 40;

    const int * p = &a;

    const int ** p2 = &p;   // 间接修改也是不允许的
    cout << *p2 << endl;
    cout << a << endl;
    cout << *p << endl;

    return 0;
}