// 只是测试一下C语言中递归调用main()
#include<stdio.h>
//int n = 3;

int main()
{
    static int n = 3;
    if (n == 0)
    {
        printf("n = %d\n", n);
        return 0;
    }
    else
    {
        printf("n = %d\n", n);
        return main(n--);
    }
}