#include<iostream>
using namespace std;

// 试了几次，函数的声明最好与定义一致，否则不匹配编译器会报错
void show_array(const int*, int n);    
void change_array(int*, int n);
int main()
{
    int arr[] = {3,4,5,7,2,8};
    const int num = sizeof(arr)/sizeof(int);
    show_array(arr, num);   // 只读

    change_array(arr, num);     // 修改数组
    show_array(arr, num);   // 只读数组，再次查看

    return 0;
}

void change_array(int arr[], int n)
{
    for (int i = 0; i < n; i++)
    {
        arr[i] = 0;         
        cout << arr[i] << ' ';
    }
    cout << endl;
}

/* 只读数组函数，保证传入的数组在函数中不被修改 */
void show_array(const int arr[], int n)
{
    for (int i = 0; i < n; i++)
        cout << arr[i] << ' ';
    cout << endl;
}